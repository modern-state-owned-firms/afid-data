
* Initial preparation
*----------------------

set more off
clear all

local date=ltrim("$S_DATE")
local date=subinstr("`date'"," ","_",2)


* Start log file
*-----------------
cap log close
log using "$LOGDIR\LOG_02a_merge_afid_energy_data_`date'.log", replace

********************************************************************************
*
*							    	Project 4193
*
* 						Merge the AFiD energy data
*
*
* -----------------------------------------------------------------------------*
* Authors: Caroline Stiel, Julia Rechlitz 
* Phone: 030-89789-514 (cs)
* Mail: cstiel@diw.de, jrechlitz@diw.de
*
* Name of the code: 02a_merge_afid_energy_data.do
* Name of the log file: LOG_02a_merge_afid_energy_data_<date>.log
* -----------------------------------------------------------------------------*


* -----------------------------------------------------------------------------*
* The code merges the following data sets:
*
* On firm level (keys: unr, jahr)
* ------------------------------------------
* - 4193_peu_2005-2016.dta
* - 4193_absatz_2005-2016.dta
* - 4193_einsp_2005-2016.dta
*
* - 4193_Netze_unique_2005-2016.dta 
*   (comes from 01a_delete_duplicates_in_evas_43312)
*
*
* On plant level (keys: unr, bnr, jahr)
* --------------------------------------------------
* - 4193_peu_2005-2016.dta
* - 4193_kraftw_unique_2005-2016.dta
*   (comes from 01b_delete_duplicates_in_evas_43311) 
* - 4193_waerme_2005-2016.dta
*
* Name of the output data set: energie0516.dta
* Path: R:\Projekt4193\Bearbeiterdaten\Programme\Aufbereitung Energiedaten\Arbeitsdateien\energie0516.dta
* ------------------------------------------------------------------------------

********************************************************************************
* 									START
********************************************************************************


********************************************************************************
* 1) Firm level
********************************************************************************

*-----------------------------------------------------------*
* 1.1 AFiD-Panel Energieunternehmen 2005-2016		        		
* (Kostenstrukturerhebung und Investitionserhebung)	    		
*-----------------------------------------------------------*

* Load data
*--------------
use "$DATADIR\4193_peu_2005-2016.dta", clear

* Number of observations per year
* ---------------------------------
tab jahr, mi

* Check for duplicates
* -------------------
sort unr jahr
duplicates report unr jahr
duplicates list unr jahr


*-----------------------------------------------------------*
* 1.2 Merge with data set "Stromabsatz- und Erlöse          
*     Energieversorger" (EVAS 43331)                        
*-----------------------------------------------------------*

use "$DATADIR\4193_absatz_2005-2016.dta", clear

* Number of observations per year
* ---------------------------------
tab jahr, mi

* Check for duplicates
* ---------------------
sort unr jahr
duplicates report unr jahr
duplicates list unr jahr


* Rename variables with identical names
* ---------------------------------------
rename wz_u absatz_wz
rename ags_u absatz_ags
rename bl_u absatz_land


* Merge with the panel data set
* -------------------------------
merge 1:1 unr jahr using "$DATADIR\4193_peu_2005-2016.dta"


* Number ofobservations per year
* ---------------------------------
tab jahr , mi
drop _merge

save "$ARBEITSDIR\TEMP1.dta", replace

*-----------------------------------------------------------*
* 1.3 Merge with data set "Einspeiseerhebung der            
*     Netzbetreiber" (EVAS XXXXXX)                          
*-----------------------------------------------------------*

use "$DATADIR\4193_einsp_2005-2016.dta", clear

* Number of observations per year
* ---------------------------------
tab jahr, mi


* Check for duplicates
* ----------------------
sort unr jahr
duplicates report unr jahr
duplicates list unr jahr


* Rename variables with identical names
* --------------------------------------
rename wz_u stromein_wz
rename ags_u stromein_ags
rename bl_u stromein_land


* Merge with the panel data set
* ------------------------------
merge 1:1 unr jahr using "$ARBEITSDIR\TEMP1.dta"


* Number of observations per year
* ---------------------------------
tab jahr _merge, mi
drop _merge


save "$ARBEITSDIR\TEMP2.dta", replace


*-----------------------------------------------------------*
* 1.4 Merge with data set "Stromversorgungserhebung der     
*      Netzbetreiber" (EVAS 43312)                          
*-----------------------------------------------------------*

use "$ARBEITSDIR\4193_Netze_unique_2005-2016.dta", clear


* Number of observations per year
* ---------------------------------
tab jahr, mi


* Check for duplicates
* ---------------------
sort unr jahr
duplicates report unr jahr
duplicates list unr jahr


* Merge with the panel data set
* -------------------------------
merge 1:1 unr jahr using "$ARBEITSDIR\TEMP2.dta"


* Number of observations per year
* ---------------------------------
tab jahr _merge, mi
drop _merge


save "$ARBEITSDIR\TEMP_unternehmen.dta", replace


********************************************************************************
* 2) Plant level
********************************************************************************

*-----------------------------------------------------------*
* 2.1 AFiD-Panel Energiebetriebe 2005-2016			        		
* (Kostenstrukturerhebung und Investitionserhebung)			    
*-----------------------------------------------------------*

use "$DATADIR\4193_peb_2005-2016.dta"

* Number of observations per year
* ---------------------------------
tab jahr, mi


* Check for duplicates
* ---------------------
sort unr bnr jahr
duplicates report unr bnr jahr
duplicates list unr bnr jahr


*-----------------------------------------------------------*
* 2.2 Wärmeerhebung											
*-----------------------------------------------------------*

use "$DATADIR\4193_waerme_2005-2016.dta", clear

* Number of observations per year
* ---------------------------------
tab jahr, mi


* Check for duplicates
* ---------------------
sort unr bnr jahr
duplicates report unr bnr jahr
duplicates list unr bnr jahr


* Merge 1:1 with the panel data set using bnr and unr as identifier
* --------------------------------------------------------------------
merge 1:1 unr bnr jahr using "$DATADIR\4193_peb_2005-2016.dta"


* Number of observations per year
* ---------------------------------
tab jahr _merge, mi
drop _merge


save "$ARBEITSDIR\TEMP5.dta", replace


*-----------------------------------------------------------*
* 2.2 Kraftwerkserhebung									
*-----------------------------------------------------------*

use "$ARBEITSDIR\4193_kraftw_unique_2005-2016.dta", clear


* Number of observations per year
* ---------------------------------
tab jahr, mi


* Check for duplicates
* ---------------------
sort unr bnr jahr
duplicates report unr bnr  jahr
duplicates list unr bnr jahr


* Merge 1:1 with the panel data set using bnr and unr as identifier
* --------------------------------------------------------------------
merge 1:1 unr bnr jahr using "$ARBEITSDIR\TEMP5.dta"


* Number of observations per year
* ---------------------------------
tab jahr _merge, mi
drop _merge

save "$ARBEITSDIR\TEMP_betriebe.dta", replace




********************************************************************************
* 3) Merge firm data with plant data
********************************************************************************

*-------------------------------------------
*  3.1) Merge
*-------------------------------------------

use "$ARBEITSDIR\TEMP_betriebe.dta", clear

* Number of observations per year
* ---------------------------------
tab jahr, mi


* Check for duplicates
* -------------------
sort unr bnr jahr
duplicates report unr bnr  jahr
duplicates list unr bnr  jahr


* Merge with m : 1
* Necessary. since some firms have several plants
* --------------------------------------------------------------------
merge m:1 unr jahr using "$ARBEITSDIR\TEMP_unternehmen.dta"


* Number of observations per year
* ---------------------------------
tab jahr _merge, mi




*-------------------------------------------
* 3.2) Analysis of the unmatched plants
*-------------------------------------------

* Which plants could not be merged, depending on their source data set?
* ----------------------------------------------------------------------
tab jahr          if _merge==1    //not merged
tab jahr peb      if _merge==1    //not merged
tab jahr evmb     if _merge==1    //not merged
tab jahr TM_066K  if _merge==1    //not merged
tab jahr TM_064   if _merge==1    //not merged


* Number of merged plants for comparison reason
* --------------------------------------------------------------
tab jahr peb      if _merge==3  // gematched
tab jahr evmb     if _merge==3  // gematched
tab jahr TM_066K  if _merge==3  // gematched
tab jahr TM_064   if _merge==3  // gematched


* Do unmerged plants have information which originats from the AFiD Plant panel?  
* ------------------------------------------------------------------------------
tab jahr peb if _merge==1, mi               //not merged
tab jahr peb if _merge==1 & TM_066K==1, mi  //not merged



*-------------------------------------------
*  3.3) Analysis of the unmatched firms
*-------------------------------------------

* Are firms which could not merged with a plant are officially recorded 
* as one-plant-company?
* ------------------------------------------------------------------------------
tab peu_ade if _merge==2, mi


* Which firms could not be merged, depending on their source data set?
* ----------------------------------------------------------------------
tab jahr            if _merge==2  //not merged
tab jahr TM_066N    if _merge==2  //not merged
tab jahr TM_070     if _merge==2  //not merged
tab jahr TM_083     if _merge==2  //not merged


* Number of merged firms for comparison reason
* -------------------------------------------------------------
tab jahr TM_066N    if _merge==3  // gematched
tab jahr TM_070     if _merge==3  // gematched
tab jahr TM_083     if _merge==3  // gematched



*-------------------------------------------
*  3.4) Clean
*-------------------------------------------
drop _merge

save "$ARBEITSDIR\TEMP_b_und_u.dta", replace



********************************************************************************
* 4) clean and save of the final data set
********************************************************************************


order unr bnr jahr peu_wz peu_ags peu_land peu evu evuk peb evb evmb TM_064 TM_066K TM_066N TM_070 TM_083
sort unr bnr jahr

* save final dataset
*-----------------------
save "$ARBEITSDIR\energie0516.dta", replace

* clean
*--------------
forvalues p=1/10{
 cap qui erase "$ARBEITSDIR\TEMP`p'.dta"
 }
erase "$ARBEITSDIR\TEMP_b_und_u.dta"
erase "$ARBEITSDIR\TEMP_betriebe.dta"
erase "$ARBEITSDIR\TEMP_unternehmen.dta"
 
********************************************************************************
* End of file
********************************************************************************
